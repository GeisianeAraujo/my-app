# Instalando os pacotes do Design System no projeto

Para instalar o pacote criado digite:

`npm i npm i @dsystem/button`

## Para utilizar os arquivos .ts

No arquivo `tsconfig.app.json`, adicione a seguinte linha:

```json
  "files": [
    "node_modules/@dsystem/button/button.component.ts"
  ],
```

## Para importar 

No `module.ts` importe:

```ts
 import { ButtonComponent } from '@dsystem/button/button.component'; 

 @NgModule({
  declarations: [
    ButtonComponent
  ],
 })

```

No HTML adicione:

```html
<dsystem-button></dsystem-button>
```
